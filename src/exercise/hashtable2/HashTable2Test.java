package exercise.hashtable2;

import static org.junit.Assert.*;

import java.util.Objects;

import org.junit.Test;

public class HashTable2Test {

	@Test
	public void key_colision(){
		HashTable2<KeyMock, Integer> table = new HashTable2<>(10);
		KeyMock a = new KeyMock(5, "a");
		KeyMock b = new KeyMock(5, "b");
		table.put(a, 10);
		table.put(b, 20);
		
		assertEquals(table.get(a), (Integer) 10);
		assertEquals(table.get(b), (Integer) 20);
	}
	
	@Test
	public void resize(){
		HashTable2<KeyMock, String> table = new HashTable2<>(2);
		KeyMock a = new KeyMock(0, "a");
		KeyMock b = new KeyMock(1, "b");
		KeyMock c = new KeyMock(2, "c");
		
		table.put(a, "0");
		table.put(b, "1");
		table.put(c, "2");
		
		assertEquals(table.get(a), "0");
		assertEquals(table.get(b), "1");
		assertEquals(table.get(c), "2");
	}
	
	@Test
	public void replaced_value(){
		HashTable2<Integer, String> table = new HashTable2<>(2);
		
		table.put(1, "2");
		table.put(1, "3");
		
		assertEquals(table.get(1), "3");
	}
	
	@Test
	public void not_existing_element(){
		HashTable2<Integer, String> table = new HashTable2<>(2);
		
		assertNull(table.get(1));
	}
	
	private class KeyMock{
		int hashCode;
		String key;
		
		KeyMock(int hashCode, String key){
			this.hashCode = hashCode;
			this.key = key;
		}
		
		@Override
		public int hashCode(){
			return hashCode;
		}
		
		@Override
		public boolean equals(Object o){
			return Objects.equals(this.key, ((KeyMock) o).key);
		}
	}
}
