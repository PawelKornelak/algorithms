package exercise.hashtable2;

import java.lang.reflect.Array;

public class HashTable2<K, V> {

	private class Entry {
		private K key;
		private V value;
		private Entry next;

		public Entry(K key, V value) {
			this.key = key;
			this.value = value;
			this.next = null;
		}

	}

	private Entry[] entries;
	private int amountOfWriten;

	public HashTable2(int length) {
		entries = (Entry[]) Array.newInstance(Entry.class, length);

	}

	public void put(K key, V value) {

		Entry newObj = new Entry(key, value);

		int index = key.hashCode() % entries.length;
		int counter = 0;
		boolean writen = false;

		if (amountOfWriten == entries.length) {
			resize();
		}

		while (counter < entries.length && writen == false) {
			if (entries[index] != null && entries[index].key.equals(key)) {
				entries[index].value = value;
				amountOfWriten++;
				writen = true;
			} else if (entries[index] == null) {
				entries[index] = newObj;
				amountOfWriten++;
				writen = true;
			} else {
				index++;
				counter++;
			}

			if (index == entries.length) {
				index = 0;
			}
		}

	}

	public V get(K key) {
		V result = null;

		int index = key.hashCode() % entries.length;
		int counter = 0;

		while (counter < entries.length && result == null) {

			if (entries[index] != null && entries[index].key.equals(key)) {
				result = entries[index].value;
			} else {
				index++;
				counter++;
			}

			if (index == entries.length) {
				index = 0;
			}
		}
		return result;

	}

	 public boolean contain(K key) {
	 boolean result = false;

		int index = key.hashCode() % entries.length;
		int counter = 0;

		while (counter < entries.length && result == false) {

			if (entries[index] != null && entries[index].key.equals(key)) {
				result = true;
			} else {
				index++;
				counter++;
			}

			if (index == entries.length) {
				index = 0;
			}
		}
		return result;
	 }

	public void resize() {
		Entry[] oldArray = entries;
		entries = (Entry[]) Array.newInstance(Entry.class, oldArray.length * 2);

		for (int i = 0; i < oldArray.length; i++) {
			if (oldArray[i] != null) {
				put(oldArray[i].key, oldArray[i].value);
			}
			
		}

		 System.out.println("resize");
		 System.out.println("old size " + oldArray.length);
		 System.out.println("new size " + entries.length);
		 System.out.println("");
	}

	public static void main(String[] args) {
		HashTable2<String, String> newTab = new HashTable2<String, String>(3);
		newTab.put("A", "Ania");
		newTab.put("B", "Basia");
		newTab.put("U", "Ula");
		newTab.put("Ag", "Agnieszka");

		System.out.println(newTab.get("A"));
		System.out.println(newTab.get("B"));
		System.out.println(newTab.get("U"));
		System.out.println(newTab.get("Ag"));
		System.out.println("If contain An: "+newTab.contain("An"));
		System.out.println("If contain Ag: "+newTab.contain("Ag"));

	}

}
