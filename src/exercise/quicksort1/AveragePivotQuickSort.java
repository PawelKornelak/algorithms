package exercise.quicksort1;

import java.util.ArrayList;
import java.util.List;

public class AveragePivotQuickSort <T extends Comparable<T>> extends QuickSort{


	@Override
	protected int pivot(List input) {
		
		
		int first =0;
		int last= input.size()-1;
		
		int middleIndex = (input.size()-1)/2;
		
		int middle=(int) input.get(middleIndex);
		
		if ((first<=middle) && (middle<=last)){return middleIndex;}
		if ((first<=last) && (last<=middle)){return last;}
		if ((middle<=first) && (first<=last)){return first;}
		if ((middle<=last) && (last<=first)){return last;}
		if ((last<=middle) && (middle<=last)){return middleIndex;}
		if ((last<=first) && (first<=middle)){return middleIndex;}

		return 0;

	}

	
	public static void main(String[] args) {

		QuickSort<Integer> qs = new AveragePivotQuickSort<Integer>();
		List<Integer> listToSort = new ArrayList<Integer>();
		List<Integer> newList = new ArrayList<Integer>();
		
		
		listToSort.add(0,5);
		listToSort.add(1,7);
		listToSort.add(2,5);
		listToSort.add(3,3);
		listToSort.add(4,2);
		listToSort.add(5,4);
		listToSort.add(6,6);
		
		System.out.println("before");
		for(int i=0;i<listToSort.size();i++){
			System.out.print(listToSort.get(i)+" " );
		}
		System.out.println("");
		newList.addAll(qs.sort(listToSort));
		System.out.println("after");
		for(int i=0;i<newList.size();i++){
			System.out.print(newList.get(i)+" " );
		}
		System.out.println("");

	}
}
