package exercise.quicksort1;

import java.util.ArrayList;
import java.util.List;

public abstract class QuickSort<T extends Comparable<T>> {

	public List<T> sort(List<T> input) {

		int pivot = pivot(input);
		List<T> leftList = new ArrayList<T>();
		List<T> tmpLeftList = new ArrayList<T>();
		List<T> rightList = new ArrayList<T>();
		List<T> tmpRrightList = new ArrayList<T>();
		List<T> sortedList = new ArrayList<T>();

		if (input.size() <= 1) {
			return input;
		}
		for (int i = 0; i < input.size(); i++) {
			if (i != pivot) {

				if (input.get(pivot).compareTo(input.get(i)) >= 0) {
					leftList.add(input.get(i));
				} else if (input.get(pivot).compareTo(input.get(i)) < 0) {
					rightList.add(input.get(i));
				}
			}
		}
		if (leftList.size() > 1) {
			tmpLeftList.addAll(sort(leftList));
			leftList = tmpLeftList;
		}
		if (rightList.size() > 1) {
			tmpRrightList.addAll(sort(rightList));
			rightList = tmpRrightList;
		}
		sortedList.addAll(leftList);
		sortedList.add(input.get(pivot));
		sortedList.addAll(rightList);

		return sortedList;
	}

	protected abstract int pivot(List<T> input);

	public static void main(String[] args) {

	}

}
