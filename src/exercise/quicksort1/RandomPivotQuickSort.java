package exercise.quicksort1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomPivotQuickSort <T extends Comparable<T>> extends QuickSort{

	


	@Override
	protected int pivot(List input) {
		int result;
		Random rd = new Random();
		result=rd.nextInt((input.size()-1));
//		System.out.println("Pivot "+result);

		return result;
	}

	
	public static void main(String[] args) {

		QuickSort<Integer> qs = new RandomPivotQuickSort<Integer>();
		List<Integer> listToSort = new ArrayList<Integer>();
		List<Integer> newList = new ArrayList<Integer>();
		
		
		listToSort.add(0,5);
		listToSort.add(1,7);
		listToSort.add(2,5);
		listToSort.add(3,3);
		listToSort.add(4,2);
		listToSort.add(5,4);
		listToSort.add(6,6);
		
		System.out.println("before");
		for(int i=0;i<listToSort.size();i++){
			System.out.print(listToSort.get(i)+" " );
		}
		System.out.println("");
		newList.addAll(qs.sort(listToSort));
		System.out.println("after");
		for(int i=0;i<newList.size();i++){
			System.out.print(newList.get(i)+" " );
		}
		System.out.println("");

	}
}
