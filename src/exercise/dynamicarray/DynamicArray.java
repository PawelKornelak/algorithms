package exercise.dynamicarray;

public class DynamicArray<T> {

	private T[] tab;
	private int firstEmpty;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DynamicArray<Integer> Obj = new DynamicArray<Integer>(1);

		Obj.append(0);
		Obj.append(1);
		Obj.append(2);
		System.out.println("Po append ");
		Obj.printArray();

		System.out.println(" Po insert 4 ");
		Obj.insert(3, 4);
		Obj.printArray();

		System.out.println(" Po prepand ");
		Obj.prepend(5);
		Obj.printArray();

		System.out.println(" Po delete element ");
		System.out.println("deleted object= " + Obj.delete(4));
		Obj.printArray();
	}

	public DynamicArray(int size) {
		tab = (T[]) new Object[size];
		firstEmpty = 0;
	}

	public void extend() {
		// int size =tab.length;
		T[] newTab = (T[]) new Object[tab.length + 1];

		// System.arraycopy( tab, 0, newTab, 0, tab.length );
		// tab = newTab;

		for (int i = 0; i < tab.length; i++) {
			newTab[i] = tab[i];
		}
		tab = newTab;
	}

	public void append(T newElement) {
		if (firstEmpty == tab.length) {
			extend();
		}

		tab[firstEmpty] = newElement;
		updateFirstEmpty();

	}

	// public void oldInsert(int index, T element){
	// while(tab.length<index+1){
	// extend();
	// }
	//
	//// if (firstEmpty==tab.length){
	//// extend();
	//// }
	// if(tab[tab.length-1]!=null){
	// extend();
	// }
	//
	// T[] newTab = (T[]) new Object[tab.length];
	// System.arraycopy( tab, 0, newTab, 0, index );
	// newTab[index]=element;
	// System.arraycopy( tab, index, newTab, index+1, tab.length-index-1 );
	// tab=newTab;
	// updateFirstEmpty();
	// }

	public void insert(int index, T element) {
		if (index > firstEmpty) {
			throw new IndexOutOfBoundsException("Index cannot be > first empty position");
		}
		if (firstEmpty == tab.length) {
			extend();
		}
		for (int i = firstEmpty; i > index; i--) {
			tab[i] = tab[i - 1];
		}
		tab[index] = element;
		updateFirstEmpty();
	}

	public void oldDelete(int index) {
		T[] newTab = (T[]) new Object[tab.length - 1];
		System.arraycopy(tab, 0, newTab, 0, index);
		System.arraycopy(tab, index + 1, newTab, index, tab.length - index - 1);
		tab = newTab;
		updateFirstEmpty();
	}

	public T delete(int index) {
		T deleteObject;
		deleteObject = tab[index];
		T[] newTab = (T[]) new Object[tab.length - 1];
		for (int i = 0; i < tab.length; i++) {
			if (i < index) {
				newTab[i] = tab[i];
			} else if (i > index) {
				newTab[i - 1] = tab[i];
			}
		}
		tab = newTab;
		updateFirstEmpty();
		return deleteObject;
	}

	public void prepend(T element) {
		insert(0, element);
	}

	public T get(int index) {
		return tab[index];
	}

	public void updateFirstEmpty() {
		firstEmpty = tab.length;
		for (int i = 0; i < tab.length; i++) {
			if (tab[i] == null) {
				firstEmpty = i;
			}
		}
	}

	public int length() {
		int counter = 0;
		for (int i = 0; i < tab.length; i++) {
			if (tab[i] != null) {
				counter++;
			}
		}
		return counter;
	}

	public void printArray() {
		for (int i = 0; i < tab.length; i++) {
			System.out.println("tab[" + i + "]= " + tab[i]);
		}
		System.out.println("firstEmpty = " + firstEmpty);
	}

}