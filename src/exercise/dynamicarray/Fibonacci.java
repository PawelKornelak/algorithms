package exercise.dynamicarray;

import java.util.HashMap;
import java.util.Map;

public class Fibonacci {
	
	public static Map<Integer,Integer> MapFib = new HashMap<Integer,Integer>();

	public static void main(String[] args) {
		System.out.println("fib iteracujnie dla 8 " + fibonaci(8));
		System.out.println("fib rek dla 8 " + fib_rek(8));
		System.out.println("fib rek Hash dla 8 " + fibRekHash(8));

	}

	public static int fibonaci(int elem) {

		int n2 = 1;
		int n1 = 0;
		int result = 1;

		// System.out.println("i=1 1");
		// System.out.println("i=2 1");
		if (elem == 1)
			return 1;// System.out.println(1);
		if (elem == 2)
			return 1;// System.out.println(1);
		if (elem > 2) {
			for (int i = 3; i < elem + 1; i++) {
				n1 = n2;
				n2 = result;
				result = n2 + n1;
				// System.out.println("i= "+i+" "+result);
			}
		}
		return result;
	}

	public static int fib_rek(int elem) {
		
		

		if (elem == 1)
			return 1;// System.out.println(1);
		if (elem == 2)
			return 1;// System.out.println(1);

		
			return fib_rek(elem - 2) + fib_rek(elem - 1);
		//return -10;
	}
	
	public static int fibRekHash(int elem) {
		int result;
		if (MapFib.containsKey(elem) ) return MapFib.get(elem);

		if (elem == 1)
			return 1;// System.out.println(1);
		if (elem == 2)
			return 1;// System.out.println(1);

		result=fib_rek(elem - 2) + fib_rek(elem - 1);
		MapFib.put(elem,result );
			return result;
		//return -10;
	}

}
