package exercise.circularqueue;

import static org.junit.Assert.*;
import org.junit.Test;

public class CircularBufferTest {

	@Test(expected = IllegalStateException.class)
	public void should_throw_exception_when_enqueue_and_queue_is_full(){
		CircularBuffer2<Integer> buffer = new CircularBuffer2<>(3);
		buffer.enqueue(1);
		buffer.enqueue(2);
		buffer.enqueue(3);
		buffer.enqueue(4);
	}
	
	@Test(expected = IllegalStateException.class)
	public void should_throw_exception_when_dequeue_and_queue_is_empty(){
		CircularBuffer2<Integer> buffer = new CircularBuffer2<>(3);
		buffer.dequeue();
	}
	
	@Test
	public void queue_works_correctly(){
		CircularBuffer2<Integer> buffer = new CircularBuffer2<>(3);
		assertTrue(buffer.isEmpty());
		assertFalse(buffer.isFull());
		buffer.enqueue(1);
		assertFalse(buffer.isEmpty());
		assertFalse(buffer.isFull());
		buffer.enqueue(2);
		assertFalse(buffer.isEmpty());
		assertFalse(buffer.isFull());
		buffer.enqueue(3);
		assertFalse(buffer.isEmpty());
		assertTrue(buffer.isFull());
		
		
		assertEquals((Integer) 1, buffer.dequeue());
		assertEquals((Integer) 2, buffer.dequeue());
		
		buffer.enqueue(4);
		
		assertEquals((Integer) 3, buffer.dequeue());
		assertEquals((Integer) 4, buffer.dequeue());
		
	}
}
