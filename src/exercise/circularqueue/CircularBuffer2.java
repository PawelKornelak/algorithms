package exercise.circularqueue;

import exercise.singlylinkedlist.SinglyLinkedList;

public class CircularBuffer2<T> {

	private T[] elements;
	private int nextRead;
	private int nextWrite;

	public CircularBuffer2(int size) {
		elements = (T[]) new Object[size];
		nextRead = -1;
		nextWrite = 0;
	}

	public T dequeue() {
		T result = null;
		if(isEmpty()){
			throw new IllegalStateException("Nothing to read");
		}
		
		if (elements[nextRead] != null) {
			result = elements[nextRead];
			elements[nextRead] = null;
			if (nextWrite == elements.length) {
				nextWrite = nextRead;
			}
		} else {
			throw new IllegalStateException("Nothing to read");
		}

		if (nextRead < elements.length - 1) {
			nextRead++;
		} else {
			nextRead = 0;
		}

		return result;

	}

	public void enqueue(T element) {
		if (nextWrite < elements.length) {
			elements[nextWrite] = element;
			if (nextRead == -1) {
				nextRead = nextWrite;
			}
		} else {
			throw new IllegalStateException("Buffer is full");
		}

		if (nextWrite + 1 < elements.length && elements[nextWrite + 1] == null) {
			nextWrite++;
		} else if (elements[0] == null) {
			nextWrite = 0;
		} else {
			nextWrite = elements.length;
		}
	}

	public void print() {
		for (int i = 0; i < elements.length; i++) {
			System.out.print(elements[i] + " ");
		}
		System.out.println("nextRead = " + nextRead + " nextWrite = " + nextWrite);
	}

	public boolean isFull() {
		boolean result = true;
		for (int i = 0; i < elements.length; i++) {
			if ((elements[i]) == null) {
				result = false;
			}
		}
		return result;
	}
	
	public boolean isEmpty() {
		boolean result = true;
		for (int i = 0; i < elements.length; i++) {
			if ((elements[i]) != null) {
				result = false;
			}
		}
		return result;
	}


	public static void main(String[] args) {
		CircularBuffer2<Integer> tab = new CircularBuffer2<Integer>(3);
		tab.dequeue();
		
//		System.out.println("is empty = " + tab.isEmpty());
//		tab.enqueue(1);
//		tab.print();
//		System.out.println("is full = " + tab.isFull());
//		tab.enqueue(2);
//		tab.print();
//		tab.enqueue(3);
//		tab.print();
//		System.out.println("is full = " + tab.isFull());
//		System.out.println("is empty = " + tab.isEmpty());
//
//		System.out.println(tab.dequeue());
//		tab.print();
//		System.out.println("is full = " + tab.isFull());
//		tab.enqueue(4);
//		tab.print();
//		System.out.println("is full = " + tab.isFull());
//		System.out.println(tab.dequeue());
//		tab.print();
//		tab.enqueue(5);
//		tab.print();
//		System.out.println(tab.dequeue());
//		tab.print();
//		tab.enqueue(6);
//		tab.print();
//		System.out.println(tab.dequeue());
//		System.out.println(tab.dequeue());
//		System.out.println(tab.dequeue());
//		tab.print();
//		System.out.println("is empty = " + tab.isEmpty());


	}

}
