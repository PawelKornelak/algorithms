package exercise.binaryheap;

public class BinaryHeap {

	private int [] tmpArray ={41,84,98,87,82,14,59,18,71,34,43,83,23,65,36};
	protected int [] heapArray;
	
	private int amountOfNodes=0;
	private int lastNodeIndex=-1;
	
	public BinaryHeap(){
		heapArray = new int [1];
	}
	
	public void enlargeHeap (){
		
		int [] newHeapArray = new int [heapArray.length+1];
		System.arraycopy(heapArray, 0, newHeapArray, 0, heapArray.length);
		heapArray= newHeapArray;
	}
	
	public void shrinkHeap (){
		
		int [] newHeapArray = new int [heapArray.length-1];
		System.arraycopy(heapArray, 0, newHeapArray, 0, heapArray.length-1);
		heapArray= newHeapArray;
	}	
	
	public int peekMax(){
		return heapArray[0];
	}
	
	public int deleteMax(){
		int returnValue;
		int indexOfParent;
		int indexOfLeftSon;
		int indexOfRightSon;
		int indexOfLargerSon;
		int tmpValue;
		
		returnValue=heapArray[0];
		
		//copy value of last leaf to root of heap and remove last leaf from heap
		heapArray[0]=heapArray[lastNodeIndex];
		heapArray[lastNodeIndex]=0;
		shrinkHeap ();
		
		//count indexes of left and right son of parent - check also if both sons exist in heap
		indexOfParent=0;
		if((2*indexOfParent + 1)<=lastNodeIndex){
			indexOfLeftSon=2*indexOfParent + 1;
		}
		else{
			indexOfLeftSon=indexOfParent;
		}
		
		if((2*indexOfParent + 2)<=lastNodeIndex){
			indexOfRightSon=2*indexOfParent + 2;
		}
		else {
			indexOfRightSon=indexOfLeftSon;
		}
				
		//count value of left and right son and chose the son which has got the larger value
		if(heapArray[indexOfLeftSon]>=heapArray[indexOfRightSon]){
			indexOfLargerSon=indexOfLeftSon;
		}
		else{
			indexOfLargerSon=indexOfRightSon;
		}
		
		//compare the value of parent and and larger son. If larger son has larger value than parent, exchange son with parent
		while(heapArray[indexOfLargerSon]>heapArray[indexOfParent]){
			tmpValue=heapArray[indexOfLargerSon];
			heapArray[indexOfLargerSon]=heapArray[indexOfParent];
			heapArray[indexOfParent]=tmpValue;
			indexOfParent=indexOfLargerSon;
			if((2*indexOfParent + 1)<=lastNodeIndex){
				indexOfLeftSon=2*indexOfParent + 1;
			}
			else{
				indexOfLeftSon=indexOfParent;
			}
			
			if((2*indexOfParent + 2)<=lastNodeIndex){
				indexOfRightSon=2*indexOfParent + 2;
			}
			else {
				indexOfRightSon=indexOfLeftSon;
			}
					
			//count value of left and right son and chose the son which has got the larger value
			if(heapArray[indexOfLeftSon]>=heapArray[indexOfRightSon]){
				indexOfLargerSon=indexOfLeftSon;
			}
			else{
				indexOfLargerSon=indexOfRightSon;
			}
		}
		
		
		return returnValue;
	}
	

	public void insert(int element){
		int newNodeIndex;
		int parentIndex;
		int tmpIndex;
		int tmpValue;
		int parentValue;
		int newNodeValue=element;
		
		if(amountOfNodes==heapArray.length){
			enlargeHeap();
		}
	
		if(amountOfNodes==0){
			heapArray[0]=element;
			amountOfNodes++;
			lastNodeIndex=0;
		}
		else{
		//put element - value of newNode in heap - as last leaf in heap	
		newNodeIndex=lastNodeIndex+1;
		lastNodeIndex=newNodeIndex;
		amountOfNodes++;
		heapArray[newNodeIndex]=newNodeValue;
		parentIndex=(int) ((newNodeIndex-1)/2);
		parentValue=heapArray[parentIndex];
		while(parentValue<newNodeValue){
			//Exchange positions between parentNode and newNode in heapArray if value of newNode is larger than value of parent
			tmpIndex=newNodeIndex;
			newNodeIndex=parentIndex;	
			parentIndex=tmpIndex;
			heapArray[newNodeIndex]=newNodeValue;
			heapArray[parentIndex]=parentValue;
			
			//find new Parent
			parentIndex=(int) ((newNodeIndex-1)/2);
			if(parentIndex<0){
				parentIndex=0;
			}
			parentValue=heapArray[parentIndex];
		}
				
		}
	}
	
	public void print(String arrayName, int [] array){
		System.out.print(arrayName+" ");
		for (int i=0;i<array.length;i++){
			System.out.print(array[i] +" ");
		}
		System.out.println("");
	}
	
	public static void main(String[] args) {
		BinaryHeap newHeap = new BinaryHeap();
		//test heap with sample from http://eduinf.waw.pl/inf/alg/001_search/0113.php

		for (int i=0;i<newHeap.tmpArray.length;i++){
			newHeap.insert(newHeap.tmpArray[i]);
		}		

		newHeap.print("tmpArray",newHeap.tmpArray);	
		newHeap.print("heapArray",newHeap.heapArray);
		System.out.println("deleteMax = "+newHeap.deleteMax());	
		newHeap.print("heapArray after deleteMax ",newHeap.heapArray);
	}

}
