package exercise.binaryheap;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import exercise.dynamicarray.DynamicArray;

public class BinaryHeapTest {
	BinaryHeap newHeap;
	static int [] testArray ={41,84,98,87,82,14,59,18,71,34,43,83,23,65,36};
	@Before
	public void setUp() throws Exception {
		
		newHeap = new BinaryHeap();
	}

	@Test
	public void insert_works_correctly() {
		
		
		for (int i=0;i<testArray.length;i++){
			newHeap.insert(testArray[i]);
		}
		assertArraysEqual(newHeap, 98,87,84,71,82,83,65,18,41,34,43,14,23,59,36);
	}

	@Test
	public void peekMax_works_correctly(){
		for (int i=0;i<testArray.length;i++){
			newHeap.insert(testArray[i]);
		}
		assertArraysEqual(newHeap, 98,87,84,71,82,83,65,18,41,34,43,14,23,59,36);
		assertEquals(newHeap.peekMax(),98);
	}
	
	@Test
	public void deleteMax_works_correctly(){
		for (int i=0;i<testArray.length;i++){
			newHeap.insert(testArray[i]);
		}
		assertArraysEqual(newHeap, 98,87,84,71,82,83,65,18,41,34,43,14,23,59,36);
		assertEquals(newHeap.deleteMax(),98);
		assertArraysEqual(newHeap, 87,82,84,71,43,83,65,18,41,34,36,14,23,59);
	}
	
	private void assertArraysEqual(BinaryHeap testHeap, int... elements){
		for(int i=0;i<elements.length;i++){
			assertEquals(elements[i], testHeap.heapArray[i]);
		}
	}
}
