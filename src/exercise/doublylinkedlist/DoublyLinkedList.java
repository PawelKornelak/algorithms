package exercise.doublylinkedlist;

public class DoublyLinkedList<T> {
	private int length;
	private Node first;
	private Node last;

	public static void main(String[] args) {

		DoublyLinkedList<Integer> newList = new DoublyLinkedList<Integer>();

		newList.append(0);
		newList.append(1);
		newList.append(2);
		newList.append(3);
		newList.append(4);
		newList.append(5);
		newList.append(6);
		newList.append(7);
		newList.append(8);
		
		// newList.prepend(6);
		// newList.insert(0, 5);
		// newList.insert(0, 3);
		// System.out.println("length "+newList.length);
		// newList.print();
		// newList.insert(1, 4);
//		newList.print();
		// System.out.println(newList.length);
//		System.out.println("del " + newList.delete(0));
//		newList.print();
		// System.out.println(newList.length);
		 System.out.println("Get 0 = "+newList.get(0));
		 System.out.println("Get 1 = "+newList.get(1));
		 System.out.println("Get 2 = "+newList.get(2));
		 System.out.println("Get 3 = "+newList.get(3));
		 System.out.println("Get 4 = "+newList.get(4));
		 System.out.println("Get 5 = "+newList.get(5));
		 System.out.println("Get 6 = "+newList.get(6));
		 System.out.println("Get 7 = "+newList.get(7));
		 System.out.println("Get 8 = "+newList.get(8));
		 

	}

	private class Node {
		T value;
		Node previous;
		Node next;

		public Node(T newValue, Node previous, Node next) {
			this.value = newValue;
			this.next = next;
			this.previous = previous;
		}
	}

	public void append(T element) {
		Node tmpNode = null;
		Node newNode;
		if (length == 0) {
			newNode = new Node(element, null, null);
			// System.out.println("dodanie pierwszego wezla");
			this.first = newNode;
		} else {
			tmpNode = this.last;

			newNode = new Node(element, tmpNode, null);
			tmpNode.next = newNode;
		}
		this.last = newNode;
		this.length++;
	}

	public void print() {

		System.out.print("Nodes Forward: ");
		Node tmpNode;

		tmpNode = first;
		while (tmpNode != null) {
			System.out.print(" " + tmpNode.value + " ");
			tmpNode = tmpNode.next;
		}
		System.out.println("");

		System.out.print("Nodes Backward: ");
		tmpNode = last;
		while (tmpNode != null) {
			System.out.print(" " + tmpNode.value + " ");
			tmpNode = tmpNode.previous;
		}
		System.out.println("");

	}

	public int length() {
		return this.length;
	}

	public T get(int index) {
		if (index >= this.length) {
			throw new IndexOutOfBoundsException(
					"Not possible index " + index + ", maximum index is " + (this.length - 1));
		}
		if(index<(this.length/2)){
			return getForward(index);
		}
		else{
			return getBackword(index);
		}

		
	}

	
	public T getBackword(int index) {
		Node tmpNode = null;
		tmpNode = this.last;

		for (int i = this.length -1; i > index; i--) {
			tmpNode = tmpNode.previous;
		}
		return tmpNode.value;
	}
	
	public T getForward(int index) {
		Node tmpNode = null;
		tmpNode = this.first;
		if (index == 0) {
			return tmpNode.value;
		}
		for (int i = 0; i < index; i++) {
			tmpNode = tmpNode.next;
		}
		return tmpNode.value;
	}
	
	
	public void deleteFirst() {
		delete(0);
	}

	public void deleteLast() {
		delete(this.length - 1);
	}

	public T delete(int index) {

		T valueOfDeleted = null;

		if (index >= this.length) {
			throw new IndexOutOfBoundsException(
					"Not possible index " + index + ", maximum index is " + (this.length - 1));
		}

		if (index == 0) {
			valueOfDeleted = this.first.value;

			if (this.last == this.first) {
				this.first = null;
				this.last = null;
			} else {
				this.first.next.previous = null;
				this.first = this.first.next;
			}
		} else {
			Node tmpNode = null;
			tmpNode = this.first;
			for (int i = 0; i < index - 1; i++) {
				tmpNode = tmpNode.next;
			}
			valueOfDeleted = tmpNode.next.value;
			tmpNode.next = tmpNode.next.next;
			if (index == this.length - 1) {
				tmpNode.next = null;
				this.last = tmpNode;
			} else {
				tmpNode.next.previous = tmpNode;
			}
		}

		this.length--;
		return valueOfDeleted;
	}

	public void insert(int index, T element) {

		if (index == 0) {
			prepend(element);
			return;
		}

		if (index == this.length) {
			append(element);
			return;
		}

		if (index > this.length) {
			throw new IndexOutOfBoundsException(
					"Not possible index " + index + ", current list to short, maximum index is " + this.length);
		}

		int counter;
		Node tmpNode = null;
		tmpNode = this.first;
		for (int i = 0; i < index - 1; i++) {
			tmpNode = tmpNode.next;
		}
		Node newNode = new Node(element, tmpNode, tmpNode.next);
		tmpNode.next.previous = newNode;
		tmpNode.next = newNode;
		this.length++;

	}

	public void prepend(T element) {
		Node tmpNode = null;
		if (length == 0) {
			Node newNode = new Node(element, null, null);
			// System.out.println("dodanie pierwszego wezla");
			this.first = newNode;
			this.last = newNode;
			this.length++;
		} else {
			tmpNode = first;
			Node newNode = new Node(element, null, tmpNode);
			tmpNode.previous = newNode;
			this.first = newNode;
			this.length++;
		}
	}
}
