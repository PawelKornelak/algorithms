package exercise.hashtable;

import java.lang.reflect.Array;



public class HashTable<K,V> {

	
	private class Entry
	{
		private K key;
		private V value;
		private Entry next;
		
		public Entry(K key,V value){
			this.key = key;
			this.value = value;
			this.next= null;
		}
		
	}
	
	
	
	private Entry [] entries;
	
	public HashTable (int length){
		entries =  (Entry[]) Array.newInstance(Entry.class, length);
	
	}
	
	
	public void put (K key, V value){
		
		
		
		Entry newObj = new Entry(key,value);
		Entry tmpObj;
		int index=key.hashCode()%entries.length;
		
		if(entries[index]==null){
			entries[index]=newObj;
		}
		else{
			tmpObj=entries[index];
			while(tmpObj.next!=null){
				tmpObj=tmpObj.next;
			}
			if(newObj.key.equals(tmpObj.key)){
				tmpObj.value=newObj.value;
			}
			else{
				tmpObj.next=newObj;
			}
			
		}
	}
	
	public V get(K key){
		V result=null;
		
		int index=key.hashCode()%entries.length;
		
		if(entries[index]==null){
			result=null;
		}
		else if(entries[index].next==null){
			result=entries[index].value;
		}
		else{
			Entry tmpObj;
			tmpObj=entries[index];
			while(tmpObj!=null && !tmpObj.key.equals(key)){
					tmpObj=tmpObj.next;
			}
			
			if(tmpObj==null){
				result=null;
			}
			else{
			result=tmpObj.value;
			}
		}
		
		return result;
	}
	
	public boolean contain(K key){
		boolean result=false;
		
		int index=key.hashCode()%entries.length;
		
		if(entries[index]==null){
			result=false;
		}
		else if(entries[index].next==null){
			if(entries[index].key.equals(key)){
			result=true;
			}
		}
		else{
			Entry tmpObj;
			tmpObj=entries[index];
			while(tmpObj!=null && tmpObj.next!=null){
					tmpObj=tmpObj.next;
					if(tmpObj.key.equals(key)){
						result=true;
					}
					
			}
	
		}
		
		return result;
	}
	
	public void resize(){
		Entry [] oldArray = entries;
		entries = (Entry[]) Array.newInstance(Entry.class, oldArray.length*2);
		
		
	
		for (int i=0;i<oldArray.length;i++ ){
			if(oldArray[i]!=null && oldArray[i].next==null){
				put(oldArray[i].key,oldArray[i].value);
			}
			else if(oldArray[i]!=null && oldArray[i].next!=null){
				Entry tmpObj;
				tmpObj=oldArray[i];
				while(tmpObj.next!=null){
					put(tmpObj.key,tmpObj.value);
					tmpObj=tmpObj.next;
				}
				put(tmpObj.key,tmpObj.value);
			}
		
		}
		for (int i=0;i<oldArray.length;i++ ){
			oldArray[i]=null;
		}
		System.out.println("");
		System.out.println("old size "+oldArray.length);
		System.out.println("new size "+entries.length);
		System.out.println("");
	}
	
	public static void main(String[] args) {
		HashTable<String,String> newTab = new HashTable<String,String>(5);
		newTab.put("A", "Ania");
		newTab.put("B", "Basia");
		newTab.put("U", "Ula");
		newTab.put("Ag", "Agnieszka");
		
		System.out.println(newTab.get("A"));
		System.out.println(newTab.get("B"));
		System.out.println(newTab.get("U"));
		System.out.println(newTab.get("Ag"));
//		System.out.println("If contain An: "+newTab.contain("An"));
//		System.out.println("If contain Ag: "+newTab.contain("Ag"));
		newTab.resize();
		System.out.println("Po resize");
		System.out.println(newTab.get("A"));
		System.out.println(newTab.get("B"));
		System.out.println(newTab.get("U"));
		System.out.println(newTab.get("Ag"));
		

	}

}
