package exercise.singlylinkedlist;

public class SinglyLinkedList<T> {

	public static void main(String[] args) {

		SinglyLinkedList<Integer> newList = new SinglyLinkedList<Integer>();
		// System.out.println(newList.length);
		newList.append(2);
		newList.append(3);
		newList.append(5);
		newList.print();
		// System.out.println("Get 0 = "+newList.get(0));
		// System.out.println("Get 1 = "+newList.get(1));
		// System.out.println("Get 2 = "+newList.get(2));
		// System.out.println("Get 3 = "+newList.get(3));
		// newList.append(3);
		// newList.print();
		// newList.insert(1,4);
		// System.out.println("len "+newList.length);
		// newList.print();
		// newList.prepend(6);
		newList.print();
		System.out.println("Delete " + newList.delete(0));
		newList.print();

	}

	int length;
	Node first;

	public SinglyLinkedList() {
		this.length = 0;
	}

	public T deleteFirst() {
		return delete(0);
	}

	public T deleteLast() {
		return delete(this.length - 1);
	}

	public T delete(int index) {
		if (index >= this.length) {
			throw new IndexOutOfBoundsException(
					"Not possible index " + index + ", maximum index is " + (this.length - 1));
		}
		T valueOfDeleted = null;

		Node tmpNode = null;
		tmpNode = this.first;
		if (index == 0) {
			valueOfDeleted = tmpNode.value;
			this.first = tmpNode.next;
			this.length--;
			return valueOfDeleted;
		}
		for (int i = 0; i < index - 1; i++) {
			tmpNode = tmpNode.next;
		}
		valueOfDeleted = tmpNode.next.value;
		tmpNode.next = tmpNode.next.next;
		this.length--;
		return valueOfDeleted;

	}

	public T get(int index) {
		if (index >= this.length) {
			throw new IndexOutOfBoundsException(
					"Not possible index " + index + ", maximum index is " + (this.length - 1));
		}

		Node tmpNode = null;
		tmpNode = this.first;
		if (index == 0) {
			return tmpNode.value;
		}
		for (int i = 0; i < index; i++) {
			tmpNode = tmpNode.next;
		}
		return tmpNode.value;
	}

	public void insert(int index, T element) {
		if (index == 0) {
			prepend(element);
			return;
		}
		if (index > this.length) {
			throw new IndexOutOfBoundsException(
					"Not possible index " + index + ", current list to short, maximum index is " + this.length);
		}

		int counter;
		Node tmpNode = null;
		tmpNode = this.first;
		for (int i = 0; i < index - 1; i++) {
			tmpNode = tmpNode.next;
		}
		Node newNode = new Node(element, null);
		newNode.next = tmpNode.next;
		tmpNode.next = newNode;
		this.length++;
	}

	public void prepend(T value) {
		Node tmpNode = null;
		if (length == 0) {
			Node newNode = new Node(value, null);
			// System.out.println("dodanie pierwszego wezla");
			this.first = newNode;
			this.length++;
		} else {
			tmpNode = first;
			Node newNode = new Node(value, null);
			this.first = newNode;
			newNode.next = tmpNode;
			this.length++;
		}
	}

	public void append(T value) {
		Node tmpNode = null;
		if (length == 0) {
			Node newNode = new Node(value, null);
			// System.out.println("dodanie pierwszego wezla");
			this.first = newNode;
			this.length++;
		} else {
			tmpNode = first;
			// for(Node current =first; current.next !=null;current =
			// current.next){
			// lastNode=current;
			// }
			while (tmpNode.next != null) {
				tmpNode = tmpNode.next;
			}

			Node newNode = new Node(value, null);
			tmpNode.next = newNode;
			this.length++;
		}
	}

	public int length() {
		return this.length;
	}

	public void print() {

		System.out.print("Nodes: ");
		Node tmpNode;

		tmpNode = first;
		while (tmpNode != null) {
			System.out.print(" " + tmpNode.value + " ");
			tmpNode = tmpNode.next;
		}
		System.out.println("");

	}

	private class Node {
		T value;
		Node next;

		public Node(T newValue, Node next) {
			this.value = newValue;
			this.next = next;
		}

	}

}
