package exercise.stack;

public interface Stack<T> {

	public boolean isEmpty();
	
	public 	void push(T element);
	public T peek();
	
	public T pop();
}
