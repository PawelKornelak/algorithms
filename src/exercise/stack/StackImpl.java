package exercise.stack;

import exercise.doublylinkedlist.DoublyLinkedList;

public class StackImpl<T> implements Stack<T> {

	private DoublyLinkedList<T> ListAsStack = new DoublyLinkedList<T>();

	public static void main(String[] args) {
		StackImpl<Integer> st = new StackImpl<Integer>();
		System.out.println("isEmpty "+st.isEmpty());
		st.push(0);
		System.out.println("isEmpty "+st.isEmpty());
		System.out.println("peek "+st.peek());
		System.out.println("isEmpty "+st.isEmpty());
		System.out.println("pop "+st.pop());
		System.out.println("isEmpty "+st.isEmpty());

	}

	@Override
	public boolean isEmpty() {
		if (ListAsStack.length() == 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void push(T element) {
		ListAsStack.append(element);
	}

	@Override
	public T peek() {

		return ListAsStack.get(ListAsStack.length() - 1);
	}

	@Override
	public T pop() {

		return ListAsStack.delete(ListAsStack.length() - 1);
	}

}
